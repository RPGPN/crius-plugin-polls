package crius_plugin_polls

import (
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

type endPollTask struct {
	m        *cc.MessageContext
	db       *sqlx.DB
	realmID  string
	platform cc.PlatformType
}

func (t endPollTask) Run() {
	err := finishPoll(t.realmID, t.platform, t.m, t.db)
	if err != nil {
		logrus.Error(err)
		return
	}
}
